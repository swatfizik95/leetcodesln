namespace leetcodesln.Tasks;

public class Task303
{
    private readonly int[] _prefixSums;

    public Task303(int[] nums)
    {
        _prefixSums = new int[nums.Length + 1];
        _prefixSums[0] = 0;

        for (int index = 0; index < nums.Length; index++)
        {
            _prefixSums[index + 1] = _prefixSums[index] + nums[index];
        }
    }

    public int SumRange(int left, int right) =>
        _prefixSums[right + 1] - _prefixSums[left];
}