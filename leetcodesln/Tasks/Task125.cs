namespace leetcodesln.Tasks;

public class Task125
{
    public bool IsPalindrome(string s)
    {
        int i = 0, j = s.Length - 1;

        while (i <= j)
        {
            if (('a' > s[i] || s[i] > 'z') && ('A' > s[i] || s[i] > 'Z') && ('0' > s[i] || s[i] > '9'))
            {
                i++;
                continue;
            }
            if (('a' > s[j] || s[j] > 'z') && ('A' > s[j] || s[j] > 'Z') && ('0' > s[j] || s[j] > '9'))
            {
                j--;
                continue;
            }

            if ('0' <= s[i] && s[i] <= '9' && s[i] != s[j]) return false;
            if (s[i] != s[j] && s[i] - 'a' != s[j] - 'A' && s[j] - 'a' != s[i] - 'A') return false;
            i++;
            j--;
        }
        
        return true;
    }
}