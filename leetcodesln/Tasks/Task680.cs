namespace leetcodesln.Tasks;

public class Task680
{
    public bool ValidPalindrome(string s)
    {
        int i = 0, j = s.Length - 1;

        while (i <= j)
        {
            if (s[i] != s[j])
                return IsPalindrome(s, i + 1, j) || IsPalindrome(s, i, j - 1);

            i++;
            j--;
        }

        return true;

        bool IsPalindrome(string sLocal, int iLocal, int jLocal)
        {
            if (iLocal >= sLocal.Length || jLocal < 0) return true;
            while (iLocal <= jLocal)
            {
                if (sLocal[iLocal] != sLocal[jLocal]) return false;

                iLocal++;
                jLocal--;
            }

            return true;
        }
    }
}