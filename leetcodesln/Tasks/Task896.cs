namespace leetcodesln.Tasks;

public class Task896
{
    public bool IsMonotonic(int[] nums)
    {
        bool isIncrease = true, isDecrease = true;
        
        for (int i = 1; i < nums.Length; i++)
        {
            isIncrease = isIncrease && nums[i] >= nums[i - 1];
            isDecrease = isDecrease && nums[i] <= nums[i - 1];
        }
        
        return isIncrease || isDecrease;
    }
}