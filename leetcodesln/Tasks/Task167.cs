namespace leetcodesln.Tasks;

public class Task167
{
    public int[] TwoSum(int[] numbers, int target)
    {
        int i = 0, j = numbers.Length - 1;

        while (i < j)
        {
            if (numbers[i] + numbers[j] == target) break;
            if (numbers[i] + numbers[j] < target) i++;
            else if (numbers[i] + numbers[j] > target) j--;
        }

        return new[] { i + 1, j + 1 };
    }
}