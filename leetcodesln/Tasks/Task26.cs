namespace leetcodesln.Tasks;

public class Task26
{
    public int RemoveDuplicates(int[] nums)
    {
        int i = 1, currIndex = 1;

        while (i < nums.Length)
        {
            if (nums[i] != nums[i - 1])
            {
                nums[currIndex] = nums[i];
                currIndex++;
            }

            i++;
        }

        return currIndex;
    }
}