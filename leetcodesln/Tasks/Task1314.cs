namespace leetcodesln.Tasks;

public class Task1314
{
    public int[][] MatrixBlockSum(int[][] mat, int k)
    {
        int columnLen = mat.Length;
        int rowLen = mat[0].Length;

        int[][] prefixMatrix = new int[columnLen + 1][];
        prefixMatrix[0] = new int[rowLen + 1];
        for (int i = 1; i < columnLen + 1; i++)
        {
            prefixMatrix[i] = new int[rowLen + 1];
            for (int j = 1; j < rowLen + 1; j++)
            {
                prefixMatrix[i][j] = prefixMatrix[i - 1][j] + prefixMatrix[i][j - 1] - prefixMatrix[i - 1][j - 1] +
                                     mat[i - 1][j - 1];
            }
        }

        int[][] answer = new int[columnLen][];

        for (int i = 0; i < columnLen; i++)
        {
            answer[i] = new int[rowLen];
            for (int j = 0; j < rowLen; j++)
            {
                answer[i][j] = SumRegion(prefixMatrix,
                    k > i ? 0 : i - k,
                    k > j ? 0 : j - k,
                    i + k > columnLen - 1 ? columnLen - 1 : i + k,
                    j + k > rowLen - 1 ? rowLen - 1 : j + k
                );
            }
        }

        return answer;
    }

    public int SumRegion(int[][] prefixMatrix, int col1, int row1, int col2, int row2)
    {
        col2++;
        row2++;

        return prefixMatrix[col2][row2] -
               prefixMatrix[col2][row1] -
               prefixMatrix[col1][row2] +
               prefixMatrix[col1][row1];
    }
}