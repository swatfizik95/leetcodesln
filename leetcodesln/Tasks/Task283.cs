namespace leetcodesln.Tasks;

public class Task283
{
    public void MoveZeroes(int[] nums)
    {
        int i = 0, lastZeroIndex = 0, zeroCount = 0;

        while (i < nums.Length)
        {
            if (nums[i] > 0 || nums[i] < 0)
            {
                nums[lastZeroIndex] = nums[i];
                lastZeroIndex++;
            }
            else zeroCount++;
            i++;
        }

        i = nums.Length - 1;
        while (zeroCount > 0)
        {
            nums[i] = 0;
            i--;
            zeroCount--;
        }
    }
}