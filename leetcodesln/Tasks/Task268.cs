namespace leetcodesln.Tasks;

public class Task268
{
    public int MissingNumber(int[] nums)
    {
        int sumCorrect = 0, sum = 0;
        for (int i = 0; i < nums.Length; i++)
        {
            sumCorrect += i;
            sum += nums[i];
        }

        sumCorrect += nums.Length;

        return sumCorrect - sum;
    }
}