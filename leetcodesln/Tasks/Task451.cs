using System.Text;

namespace leetcodesln.Tasks;

public class Task451
{
    public string FrequencySort(string s)
    {
        if (s.Length < 2) return s;

        var letterSums = new Dictionary<char, int>(s.Length);
        foreach (var symbol in s)
        {
            if (letterSums.ContainsKey(symbol)) letterSums[symbol]++;
            else letterSums.Add(symbol, 1);
        }

        var letterListSums = new List<char>[s.Length + 1];
        foreach (var letterSum in letterSums)
        {
            if (letterListSums[letterSum.Value]?.Any() == true) letterListSums[letterSum.Value].Add(letterSum.Key);
            else letterListSums[letterSum.Value] = new List<char> { letterSum.Key };
        }

        var sb = new StringBuilder();
        for (int i = letterListSums.Length - 1; i >= 1; i--)
            if (letterListSums[i]?.Any() == true)
                foreach (var sum in letterListSums[i])
                    sb.Append(new string(sum, i));

        return sb.ToString();
    }
}