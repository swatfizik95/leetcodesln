namespace leetcodesln.Tasks;

public class Task11
{
    public int MaxArea(int[] height)
    {
        int i = 0, j = height.Length - 1, max = 0;

        while (i <= j)
        {
            int currMax = (j - i) * (height[i] > height[j] ? height[j] : height[i]);
            if (currMax > max) max = currMax;
            
            if (height[i] > height[j]) j--;
            else i++;
        }
        
        return max;
    }
}