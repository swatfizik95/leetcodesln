namespace leetcodesln.Tasks;

public class Task560
{
    public int SubarraySum(int[] nums, int k)
    {
        int count = 0;
        int curr = 0;
        var dict = new Dictionary<int, int> { { 0, 1 } };
        foreach (var num in nums)
        {
            curr += num;
            if (dict.ContainsKey(curr - k)) count += dict[curr - k];
            if (dict.ContainsKey(curr)) dict[curr]++;
            else dict[curr] = 1;
        }

        return count;
    }
}