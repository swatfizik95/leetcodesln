namespace leetcodesln.Tasks;

public class Task977
{
    public int[] SortedSquares(int[] nums)
    {
        int i = 0, j = nums.Length - 1, k = nums.Length - 1;
        int[] result = new int[nums.Length];
        
        while (i <= j)
        {
            if (Math.Abs(nums[i]) >= Math.Abs(nums[j]))
            {
                result[k] = nums[i] * nums[i];
                i++;
            }
            else
            {
                result[k] = nums[j] * nums[j];
                j--;
            }

            k--;
        }
        
        return result;
    }
}