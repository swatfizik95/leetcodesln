namespace leetcodesln.Tasks;

public class Task347
{
    public int[] TopKFrequent(int[] nums, int k)
    {
        var dict = new Dictionary<int, int>();
        int maxCount = 1;

        for (int i = 0; i < nums.Length; i++)
        {
            if (dict.ContainsKey(nums[i]))
            {
                dict[nums[i]]++;
                if (dict[nums[i]] > maxCount) maxCount = dict[nums[i]];
            }
            else dict[nums[i]] = 1;
        }

        var numsFreq = new List<int>[maxCount];

        foreach (var kvp in dict)
        {
            int currCount = kvp.Value - 1;
            if (numsFreq[currCount] == null) numsFreq[currCount] = new List<int> { kvp.Key };
            else numsFreq[currCount].Add(kvp.Key);
        }


        int[] result = new int[k];

        int j = 0;
        for (int i = numsFreq.Length - 1; i >= 0; i--)
        {
            if (numsFreq[i] != null)
            {
                foreach (var num in numsFreq[i])
                {
                    result[j] = num;
                    j++;
                    if (j >= k) return result;
                }
            }
        }

        return result;
    }
}