namespace leetcodesln.Tasks;

public class Task912
{
    public int[] SortArray(int[] nums)
    {
        const int offset = 5 * 10_000;

        int[] arr = new int[offset * 2 + 1];

        foreach (var num in nums)
            arr[num + offset]++;

        int index = 0;
        for (int i = 0; i < arr.Length; i++)
        {
            for (int j = 0; j < arr[i]; j++)
            {
                nums[index] = i - offset;
                index++;
            }
        }

        return nums;
    }
}