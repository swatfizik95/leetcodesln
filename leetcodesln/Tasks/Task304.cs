namespace leetcodesln.Tasks;

public class Task304
{
    private int[][] _prefixMatrix;

    public Task304(int[][] matrix)
    {
        _prefixMatrix = new int[matrix.Length + 1][];
        _prefixMatrix[0] = new int[matrix[0].Length + 1];
        for (int i = 1; i < matrix.Length + 1; i++)
        {
            _prefixMatrix[i] = new int[matrix[0].Length + 1];
            for (int j = 1; j < matrix[0].Length + 1; j++)
            {
                _prefixMatrix[i][j] = _prefixMatrix[i - 1][j] + _prefixMatrix[i][j - 1] - _prefixMatrix[i - 1][j - 1] +
                                      matrix[i - 1][j - 1];
            }
        }
    }

    public int SumRegion(int row1, int col1, int row2, int col2)
    {
        row2++;
        col2++;

        return _prefixMatrix[row2][col2] -
               _prefixMatrix[row2][col1] -
               _prefixMatrix[row1][col2] +
               _prefixMatrix[row1][col1];
    }
}