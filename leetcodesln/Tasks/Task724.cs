namespace leetcodesln.Tasks;

public class Task724
{
    public int PivotIndex(int[] nums)
    {
        int sumRight = nums.Sum(), sumLeft = 0;

        for (int index = 0; index < nums.Length; index++)
        {
            sumRight -= nums[index];

            if (sumRight == sumLeft) return index;

            sumLeft += nums[index];
        }

        return -1;
    }
}